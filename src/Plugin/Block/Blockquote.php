<?php

namespace Drupal\uw_cbls_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a 'CustomBlock' block.
 *
 * @Block(
 *  id = "uw_cbl_blockquote",
 *  admin_label = @Translation("Blockquote"),
 * )
 */
class Blockquote extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the form from the parent.
    $form = parent::blockForm($form, $form_state);

    // Load in the config for this block.
    $config = $this->getConfiguration();

    // The quote text form element.
    $form['uw_bq_quote_text'] = array (
      '#type' => 'text_format',
      '#title' => $this->t('Quote text'),
      '#description' => $this->t('Enter to the quote text.'),
      '#default_value' => isset($config['uw_bq_quote_text']) ? $config['uw_bq_quote_text']['value'] : '',
      '#format' => 'uw_tf_standard',
    );

    // The quote attribution form element.
    $form['uw_bq_quote_attribution'] = array (
      '#type' => 'text_format',
      '#title' => $this->t('Quote attribution'),
      '#description' => $this->t('Enter to the quote attribution.'),
      '#default_value' => isset($config['uw_bq_quote_attribution']) ? $config['uw_bq_quote_attribution']['value'] : '',
      '#format' => 'uw_tf_standard',
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Set the quote text in config.
    $this->setConfigurationValue('uw_bq_quote_text', $form_state->getValue('uw_bq_quote_text'));

    // Set the quote attribution in config.
    $this->setConfigurationValue('uw_bq_attribution', $form_state->getValue('uw_bq_attribution'));
  }

  public function build() {

    // Load in the config for this block.
    $config = $this->getConfiguration();

    // If there is text, add the variable.
    if (!empty($config['uw_bq_quote_text'])) {
      $blockquote['text'] = [
        '#type' => 'processed_text',
        '#text' => $config['uw_bq_quote_text']['value'],
        '#format' => $config['uw_bq_quote_text']['format'],
      ];
    }

    // If there is text, add the variable.
    if (!empty($config['uw_bq_quote_attribution'])) {
      $blockquote['text'] = [
        '#type' => 'processed_text',
        '#text' => $config['uw_bq_quote_attribution']['value'],
        '#format' => $config['uw_bq_quote_attribution']['format'],
      ];
    }

    return [
      '#theme' => 'uw_cbl_blockquote',
      '#blockquote' => $blockquote,
    ];
  }
}