<?php

namespace Drupal\uw_cbls_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a 'CustomBlock' block.
 *
 * @Block(
 *  id = "uw_cbl_copy_text",
 *  admin_label = @Translation("Copy text"),
 * )
 */
class CopyText extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['uw_copy_text'] = array (
      '#type' => 'text_format',
      '#title' => $this->t('Copy text'),
      '#description' => $this->t('Enter to the copy text.'),
      '#default_value' => isset($config['uw_copy_text']) ? $config['uw_copy_text']['value'] : '',
      '#format' => 'uw_tf_standard',
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('uw_copy_text', $form_state->getValue('uw_copy_text'));
  }

  public function build() {

    $config = $this->getConfiguration();

    if (!empty($config['uw_copy_text'])) {
      $copy_text = [
        '#type' => 'processed_text',
        '#text' => $config['uw_copy_text']['value'],
        '#format' => $config['uw_copy_text']['format'],
      ];
    }

    return [
      '#theme' => 'uw_cbl_copy_text',
      '#copy_text' => $copy_text,
    ];
  }
}